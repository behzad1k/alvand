import React from 'react';
import './Home.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from "react-responsive-carousel";
import Slider1 from './Slider1.jpg';
import Slider2 from './Webp.net-resizeimage.jpg';
import News1 from './news1.png';
import News2 from './news2.png';
import business from './aboutus.png';
import Service1 from './Service1.png';
import Service2 from './Service2.png';
import Service3 from './Service3.png';
import Icon1 from './imageedit_2_4967833679.png';
import Icon2 from './imageedit_4_6838381843.png';
import Icon3 from './imageedit_7_4530834671.png';
import Particles from 'react-particles-js';
import particlesConfig from './configParticles';
import vid from './AlvandVid.mp4'
import Zoom from 'react-reveal/Zoom';
import Fade from "react-reveal/Fade";
import Bounce from "react-reveal/Bounce";
import HeadShake from "react-reveal/HeadShake";


function Home() {
    return (
        <div className="homeBody">

                <header>
                    <video loop autoPlay muted id="myvid">
                        <source src={vid} type="video/mp4"/>
                    </video>
                    {/*<Carousel autoPlay={true} showArrows={true} dynamicHeight={false} infiniteLoop={true} showStatus={false} showThumbs={false} stopOnHover={true} transitionTime={1000} emulateTouch={true}>*/}
                    {/*    <div>*/}
                    {/*        <img src={Slider1}/>*/}
                    {/*        <p className="legend">وقتی پول متوجه می‌شود در دستان خوبی قرار دارد، تمایل پیدا می‌کند در آن دستان بماند و چندبرابر شود.</p>*/}
                    {/*        <div className="Alvand">*/}
                    {/*            <h1>*/}
                    {/*                شرکت کارگزاری ارزش آفرین الوند*/}
                    {/*            </h1>*/}
                    {/*            <h3>*/}
                    {/*                پیشرو در ارائه خدمات بازار سرمایه ایران*/}
                    {/*            </h3>*/}
                    {/*            <a href="#"><button onClick="">نقطه شروع بورس</button></a>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*    <div>*/}
                    {/*        <img src={Slider2}/>*/}
                    {/*        <p className="legend">رویا، تغییر، سرمایه‌گذاری</p>*/}
                    {/*    </div>*/}
                    {/*</Carousel>*/}
                </header>
                <div className="BBBB">
                    <Particles id="Part" params={particlesConfig} />

                    <div className="MenuContainer">
                        <Fade left>
                        <div className="IconContainer">
                            <img src={Icon1}/>
                            <div className="Line"></div>
                            <h3>آپ تحلیل</h3>
                            <p>
                                تحلیل اپ، ارائه تحلیل های کیفی، دقیق و همه جانبه از بازار بورس ایران، برای گزینش و چینش یک سبد بهینه و کارآمد.

                            </p>
                        </div>
                        </Fade>
                        <Fade left delay={500}>
                        <div className="IconContainer">
                            <img src={Icon2}/>
                            <div className="Line"></div>
                            <h3>آموزشگاه</h3>
                            <p>
                                شصدینصد
                                ششپدصهنیشی
                                شدپنتصیصش
                            </p>
                        </div>
                        </Fade>
                        <Fade left delay={1000}>
                        <div className="IconContainer">
                            <img src={Icon3}/>
                            <div className="Line"></div>
                            <h3>رصد بازار</h3>
                            <p>
                                شصدینصد
                                ششپدصهنیشی
                                شدپنتصیصش
                            </p>
                        </div>
                        </Fade>
                        <Fade left delay={1500}>
                        <div className="IconContainer">
                            <img src={Icon1}/>
                            <div className="Line"></div>
                            <h3>کارگزاری</h3>
                            <p>
                                شصدینصد
                                ششپدصهنیشی
                                شدپنتصیصش
                            </p>
                        </div>
                        </Fade>
                        <Fade left delay={2000}>
                        <div className="IconContainer">
                            <img src={Icon2}/>
                            <div className="Line"></div>
                            <h3>فروشگاه</h3>

                            <p>
                                شصدینصد
                                ششپدصهنیشی
                                شدپنتصیصش
                            </p>
                        </div>
                        </Fade>


                    </div>
                    <Zoom>
                    <div className="newsContainer">
                        <h3 id="newsTitle">اخبار و رویداد ها</h3>
                        <Carousel className="newsBox" autoPlay={true} showArrows={true} infiniteLoop={true}
                                  showStatus={false} showThumbs={false} stopOnHover={true} emulateTouch={true}>
                            <div className="newsPost">
                                <img src={News1}/>
                                <div className="newsCaption">
                                    <a href="#">
                                        <h3>فرا رسیدن فصل طبیعت و شروع سال 1400 مبارک باد</h3>
                                    </a>
                                    <time>01/01/1400</time>
                                    <p>
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                        گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                                    </p>
                                </div>
                            </div>
                            <div className="newsPost">
                                <img src={News2}/>
                                <div className="newsCaption">
                                    <a href="#">
                                        <h3>فرا رسیدن فصل طبیعت و شروع سال 1400 مبارک باد</h3>
                                    </a>
                                    <time>01/01/1400</time>
                                    <p>
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                        گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                                    </p>
                                </div>
                            </div>
                        </Carousel>
                    </div>
                    </Zoom>
                </div>
            <Bounce>
                <div className="aboutUsContainer" dir="rtl">
                    <div className="aboutUs">

                        <img src={business}/>
                        <div className="aboutUsCaption">
                            <h1>
                                درباره کارگزاری
                            </h1>
                            <p>
                                رکت کارگزاری ارزش آفرین الوند در تاریخ 1373/10/19 تحت نام بدره سهام و شماره 110391 در
                                اداره ثبت شرکتها باسرمایه اولیه یکصدمیلیون (100،000،000) ريال توسط جمعی از افراد بخش
                                خصوصی بنیان گذاری شد و طی چند مرحله سرمایه خود را به بیست و پنج میلیارد (25،000،000،000)
                                ريال افزایش داد. در اواخر سال 1389 با جابجایی سهامداران، مدیریت جدیدی سکان رهبری
                                کارگزاری را بدست گرفته است و با تغییر نام شرکت از بدره سهام به ارزش آفرین الوند و گسترش
                                ایستگاه های معاملاتی به7 ایستگاه (6 ایستگاه در تهران ،1 ایستگاه در کرج) قدم به راهی نوین
                                در جهت گسترش کمی و کیفی بازار سرمایه گذاشته است.
                                این کارگزاری در تلاش است تا با تحکیم موازین بازار سرمایه، فرهنگ سهامداری را دربین آحاد
                                مردم نهادینه نموده و سرمایه گذاری را به عنوان یک پس انداز و منبع درآمد واحدهای صنعتی
                                معرفی نماید.
                            </p>
                        </div>
                    </div>
                </div>
            </Bounce>
        </div>
    );
}

export default Home;
